<?php

/*
Plugin Name: Envato FlexSlider
Plugin URI: 
Description: A simple plugin that integrates FlexSlider (http://flex.madebymufffin.com/) with WordPress using custom post types!
Author: Joe Casabona
Version: 0.5
Author URI: http://www.casabona.org
*/

/*Some Set-up*/
define('EFS_PATH', plugins_url() . '/' . plugin_basename( dirname(__FILE__) ) . '/' );
define('EFS_NAME', "Envato FlexSlider");
define ("EFS_VERSION", "0.5");

/*Files to Include*/
require_once('slider-img-type.php');


/*Add the Javascript/CSS Files!*/
wp_enqueue_script('flexslider', EFS_PATH.'jquery.flexslider-min.js', array('jquery'));
wp_enqueue_style('flexslider_css', EFS_PATH.'flexslider.css');


/*Add the Hooks to place the javascript in the header*/

function efs_script(){

print '<script type="text/javascript" charset="utf-8">
  jQuery(window).load(function() {
    jQuery(\'.flexslider\').flexslider();
  });
</script>';

}

add_action('wp_head', 'efs_script');

function efs_get_slider(){
	
	$slider= '<div class="flexslider">
	  <ul class="slides">';

	//$efs_query= "cat=57&orderby=date&order=DESC& posts_per_page =4";
	//query_posts($efs_query);
	
	query_posts(array('posts_per_page' => 4, 'cat' => 57, 'order'=>'date'));

	if (have_posts()) {
		$post_array = array();
		$dates = array();

		while (have_posts()) { 
			the_post(); 
			$date = get_post_time('U', true);
			$category = get_the_category();

			$content = get_the_content();
			$link = preg_match_all( '/href\s*=\s*[\"\']([^\"\']+)/', $content, $links);
    			$link = $links[1][0];

			$permalink = get_permalink( $page->ID );
			$title = get_the_title($page->ID);
			$copyright = get_post_meta(get_the_ID(), "copyright", true);
			$image = get_the_post_thumbnail($page->ID, 'slider-home');

    			if (empty($link)) {
    				$link = get_permalink( $page->ID );
    			}
			
			$post_array[$date] = array(
					"date" => $date,
					"category" => $category,
					"content" => $content,
					"link" => $link,
					"permalink" => $permalink,
					"title" => $title,
					"copyright" => $copyright,
					"image" => $image
			);
			
			array_push($dates, $date);
		}


		rsort($dates, SORT_NUMERIC);

		for ($i = 0; $i < count($dates); $i++) {
			$date = $dates[$i];
			$post = $post_array[$date];
			$category = $post["category"];
			$content = $post["content"];
			$link = $post["link"];
			$permalink = $post["permalink"];
			$title = $post["title"];
			$copyright = $post["copyright"];
			$image = $post["image"];

			$slider.='<li><div class="image_slider">
                     		<a href="'. $link .'">'. $image .'</a>
				</div>
                		<div class="Content">
				<div class="Texto_Slide">
					<p class="cat">'. $category[0]->cat_name. '</p>
						<h2><a href="'. $permalink .'">'. $title .'</a></h2>
						<p>'. $content.'</p>
						<!--<a href="'. $permalink .'">Read More</a>-->
						<div class="copyright"><p>'. $copyright .'</p></div>
					</div>
                		</div></li>';
			
		} 

	}
	 wp_reset_query();


	$slider.= '</ul>
	</div>';
	
	return $slider;
}


/**add the shortcode for the slider- for use in editor**/

function efs_insert_slider($atts, $content=null){

$slider= efs_get_slider();

return $slider;

}


add_shortcode('ef_slider', 'efs_insert_slider');



/**add template tag- for use in themes**/

function efs_slider(){

	print efs_get_slider();
}


?>